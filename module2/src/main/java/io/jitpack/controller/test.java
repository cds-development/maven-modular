package io.jitpack.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class test {
    public static String GREETING = "Hello World!";

    @GetMapping
    public String greeting(){
        return GREETING;
    }

}
